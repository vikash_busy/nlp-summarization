/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vikash
 */
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class LexRank_ACS {

    protected StanfordCoreNLP pipeline;
    public static HashMap<String,Integer> vocabulary = new HashMap<String,Integer>();
    public static HashMap<String,Double> idf = new HashMap<String,Double>();
    public static HashMap<String,Integer> hash_x = new HashMap<String,Integer>();
    public static HashMap<String,Integer> hash_y = new HashMap<String,Integer>();
    public static HashMap<String,Integer> hash_xy = new HashMap<String,Integer>();
    public static int no_of_words = 367;
    public static int lines = 22;
    public static ArrayList<ArrayList <Double>> g = new ArrayList<ArrayList<Double>>(); // The g maintains the adjacency matrix
    public static ArrayList<String> gmap = new ArrayList<String>();

    public LexRank_ACS() {
        
        Properties props;
        props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma");

        this.pipeline = new StanfordCoreNLP(props);
    }

    public List<String> lemmatize(String documentText)
    {
        List<String> lemmas = new LinkedList<>();
        // Create an empty Annotation just with the given text
        Annotation document = new Annotation(documentText);
        // run all Annotators on this text
        this.pipeline.annotate(document);
        // Iterate over all of the sentences found
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for(CoreMap sentence: sentences) {
            // Iterate over all tokens in a sentence
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                // Retrieve and add the lemma for each word into the
                // list of lemmas
                lemmas.add(token.get(LemmaAnnotation.class));
            }
        }
        return lemmas;
    }
    
    public static String read_from_file(String input_file) throws FileNotFoundException, IOException
    {
        
        BufferedReader br;
        br = new BufferedReader(new FileReader(input_file));
        String input_str = "";
        String line;
        while ((line = br.readLine()) != null) {
                    input_str = input_str +" "+ line.toLowerCase();
		}
        
        return input_str;
    }

    public static HashSet add_lemmas(String input_doc) throws IOException
    {
        HashSet hs = new HashSet();
        System.out.println("Starting Stanford Lemmatizer");
      //String text1 = "he is watching the greatest tennis game in the history. Tennis is an excellent game";
        String text = read_from_file(input_doc);
        LexRank_ACS slem = new LexRank_ACS();
        List<String> lis = slem.lemmatize(text);
//        int siz = slem.lemmatize(text1).size();
//        String temp_s;
//        for(int i=0;i<siz;i++)
//        {
//            temp_s = lis.get(i);
//            if(temp_vocabulary.containsKey(lis.get(i)))
//            {
//                /// ditch nothing needed to be done
//            }
//            else
//            {
//                if(!(temp_s.equals("-lsb-") || temp_s.equals("-rsb-")))
//                temp_vocabulary.put(temp_s,1);
//            }
//        } 
        hs.addAll(lis);
//        System.out.println(lis);
//        System.out.println(hs);
//        System.out.println(lis.size() + ","+hs.size());
        
        return hs;
    }
    
    public static void merge_hashes(HashSet hs)
    {
        Iterator iterator = hs.iterator(); 
        String temp;
      // check values
      while (iterator.hasNext()){
          temp = (String) iterator.next();
         if(vocabulary.containsKey(temp))
         {
              if(!(temp.equals("-lsb-") || temp.equals("-rsb-")))
               vocabulary.put(temp,vocabulary.get(temp)+1);
         }
         
         else
         {
               if(!(temp.equals("-lsb-") || temp.equals("-rsb-")))
               vocabulary.put(temp,1);
         }
      }
    }
    
    public static void calculate_idfs() throws IOException
    {
        //add_lemmas("/home/bhargava/corpus_acs/Airship");
        String path = "../corpus_acs/"; 
         String files;
         File folder = new File(path);
         File[] listOfFiles = folder.listFiles();
         int N = listOfFiles.length;
         no_of_words = N;
        for (File listOfFile : listOfFiles) {
            BufferedReader br;
                //System.out.println("Reading File: "+listOfFile.getName());
                String corpus_file = path + listOfFile.getName();       // Reading an input from a text file
                
                merge_hashes(add_lemmas(corpus_file)); 
                System.out.println("Size: " + vocabulary.size());
        }
        int temp;
        double n = N;
        for(String key: vocabulary.keySet())
        {
            temp = vocabulary.get(key);
            if(temp == 0) idf.put(key,Math.log(n));
            else idf.put(key,Math.log(n/temp));
        }
        
        try (PrintWriter printwriter = new PrintWriter("../vocabulary")) {
            for (String key : idf.keySet()) {
                printwriter.print(key);
                printwriter.print(" ");
                printwriter.println(idf.get(key));
            }
            
        printwriter.close();
        }
    }
    
    public static void read_to_idf(String input_file) throws FileNotFoundException, IOException
    {
        idf.clear();
        BufferedReader br = new BufferedReader(new FileReader(input_file));
        String line;
        String[] ss;
         while ((line = br.readLine()) != null) {
                    ss = line.split(" ");
                    idf.put(ss[0], Double.parseDouble(ss[1]));
		}
    }
    
    public static ArrayList<String> read_sentence_file(String input_file) throws FileNotFoundException, IOException
    {
        ArrayList<String> result;
        
        BufferedReader br;
        br = new BufferedReader(new FileReader(input_file));
        String input_str = "";
        String line;
        while ((line = br.readLine()) != null) {
                    input_str = input_str + line;
		}
        
        String[] all_strs = input_str.split("\\.");
        List<String> l = Arrays.<String>asList(all_strs);
        
        result = new ArrayList<String>(l);
        
        return result;
    }
    
    
    
    public static double idf_cosine(String x,String y)
    {
        hash_x.clear();
        hash_y.clear();
        hash_xy.clear();
        LexRank_ACS slem = new LexRank_ACS();
        List<String> X = slem.lemmatize(x);
        
        LexRank_ACS slem1 = new LexRank_ACS();
        List<String> Y = slem1.lemmatize(y);
        
        for (String X1 : X) {
            if(hash_x.containsKey(X1))
            {
                hash_x.put(X1,hash_x.get(X1)+1);
            }
            else
            {
                if(!(X1.equals("-lsb-") || X1.equals("-rsb-")))
                {
                    hash_x.put(X1, 1);
                    hash_xy.put(X1,1);
                }
            }
        }
        
        for (String Y1 : Y) {
            if(hash_y.containsKey(Y1))
            {
                hash_y.put(Y1,hash_y.get(Y1)+1);
            }
            else
            {
                if(!(Y1.equals("-lsb-") || Y1.equals("-rsb-")))
                {
                    hash_y.put(Y1, 1);
                    hash_xy.put(Y1,1);
                }
            }
        }
        
        //calculating left half of the denominator  .... :P
        double sum_denx = 0.0;
        double tf;
        double id;
        for(String xx : hash_x.keySet())
        {
            tf = hash_x.get(xx);
            if(idf.containsKey(xx)) id = idf.get(xx);
            else id = Math.log(no_of_words);
            sum_denx = sum_denx + tf*tf*id*id;
        }
        
        double sum_deny = 0.0;
        for(String yy : hash_y.keySet())
        {
            tf = hash_y.get(yy);
            if(idf.containsKey(yy)) id = idf.get(yy);
            else id = Math.log(no_of_words);
            sum_deny = sum_deny + tf*tf*id*id;
        }
        
        //calculating numerator
        double sum_num = 0.0;
        double tf_x;
        double tf_y;
        for(String xy : hash_xy.keySet())
        {
            if(hash_y.containsKey(xy)) tf_y = hash_y.get(xy);
            else tf_y = 0.0;
            if(hash_x.containsKey(xy)) tf_x = hash_x.get(xy);
            else tf_x = 0.0;
            
            tf = tf_x*tf_y;
            if(idf.containsKey(xy)) id = idf.get(xy);
            else id = Math.log(no_of_words);
            sum_num = sum_num + tf*id*id;
        }
        
        double numerator = sum_num;
        double denominator = Math.sqrt(sum_denx*sum_deny);
        
        double result = numerator/denominator;
        
        return result;
    }
    
    
    
    public static ArrayList<ArrayList <Double>> create_graph(String input_file) throws IOException
    {
        ArrayList<String> ss = read_sentence_file(input_file);
        gmap = ss;
        
        g.clear();
        int n = ss.size();
        int i,j;
        String x,y;
        double edge_weight = 0.0;
        int z;
        for(i=0;i<n;i++)
        {
            ArrayList<Double> temp = new ArrayList<Double>();
            x = ss.get(i);
            for(j=0;j<n;j++)
            { 
                y = ss.get(j);
                edge_weight = idf_cosine(x,y);
                temp.add(edge_weight);
            }
            g.add(temp);
        }
        
        return g;
    }
    
    public static void update_edge(int i,int j, Double d)
    {
        g.get(i).set(j, d);
    }
    
    public static Double edge(int i,int j)
    {
        return g.get(i).get(j);
    }
    
    public static void print_graph()
    {
        int n = g.size();
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++)
            {
                System.out.print(g.get(i).get(j) + " ");
            }
            System.out.println();
        }
    }
    
    public static ArrayList<ArrayList <Double>> transpose(ArrayList<ArrayList <Double>> m)
    {
        ArrayList<ArrayList <Double>> t = new ArrayList<ArrayList<Double>>();
        int n = m.size(); /// In the complete problem we assume that a matrix is an nxn square matrix
        
        for(int i=0;i<n;i++){
            ArrayList<Double> temp = new ArrayList<Double>();
            for(int j=0;j<n;j++){
                temp.add(m.get(j).get(i));
            }
            t.add(temp);
        }
            
        return t;
    }
    
    public static ArrayList<ArrayList<Double>> take_input() throws IOException
    {
        ArrayList<ArrayList <Double>> t = new ArrayList<ArrayList<Double>>();
        System.out.println("The size of the square matrix: ");
        
        BufferedReader breader = new BufferedReader(new InputStreamReader(System.in));
        String str;
        str=breader.readLine();
        
        int n = Integer.parseInt(str);
        int kk;
        
        for(int i=0;i<n;i++){
            ArrayList<Double> temp = new ArrayList<Double>();
            str = breader.readLine();
            String[] ss = str.split(" ");
            kk = ss.length;
            if(kk == n) {
                for(int j=0;j<n;j++)
                    temp.add(Double.parseDouble(ss[j]));
            }
            else
            {
                System.out.println("u have entered it wrong");
            }
            //t.add(temp);
            t.add(i, temp);
            
        }
        
        return t;
    }
    
    public static ArrayList<ArrayList<Double>> take_file_input(String input_file) throws FileNotFoundException, IOException
    {
         ArrayList<ArrayList <Double>> t = new ArrayList<ArrayList<Double>>();
        
        
        BufferedReader breader = new BufferedReader(new FileReader(input_file));
        String str;
        str=breader.readLine();
        
        int n = Integer.parseInt(str);
        int kk;
        
        for(int i=0;i<n;i++){
            ArrayList<Double> temp = new ArrayList<Double>();
            str = breader.readLine();
            String[] ss = str.split(" ");
            kk = ss.length;
            if(kk == n) {
                for(int j=0;j<n;j++)
                    temp.add(Double.parseDouble(ss[j]));
            }
            else
            {
                System.out.println("u have entered it wrong");
            }
            //t.add(temp);
            t.add(i, temp);
            
        }
        
        //System.out.println(t);
        return t;
    }
    
    public static ArrayList<ArrayList<Double>> add_matrix(ArrayList<ArrayList<Double>> m1,ArrayList<ArrayList<Double>> m2)
    {
        ArrayList<ArrayList<Double>> m = new ArrayList<ArrayList<Double>>();
        int n1 = m1.size();
        int n2 = m2.size();
        int n = n1;
        if(n1 == n2)
        {
            // addition is compatible
            for(int i=0;i<n;i++)
            {
                ArrayList<Double> temp = new ArrayList<Double>();
                for(int j=0;j<n;j++)
                {
                    temp.add(m1.get(i).get(j)+m2.get(i).get(j));
                }
                m.add(temp);
            }
        }
        
        else{
            System.out.println("Incompatible types for matrix addition. Please check them ");
            exit(0);
        }
        
        return m;
    }
    
    public static ArrayList<ArrayList<Double>> sub_matrix(ArrayList<ArrayList<Double>> m1,ArrayList<ArrayList<Double>> m2)
    {
        ArrayList<ArrayList<Double>> m = new ArrayList<ArrayList<Double>>();
        int n1 = m1.size();
        int n2 = m2.size();
        int n = n1;
        if(n1 == n2)
        {
            // addition is compatible
            for(int i=0;i<n;i++)
            {
                ArrayList<Double> temp = new ArrayList<Double>();
                for(int j=0;j<n;j++)
                {
                    temp.add(m1.get(i).get(j)-m2.get(i).get(j));
                }
                m.add(temp);
            }
        }
        
        else{
            System.out.println("Incompatible types for matrix addition. Please check them ");
            exit(0);
        }
        
        return m;
    }
    
    public static double dot(ArrayList<Double> v1, ArrayList<Double> v2) // Computes the dot product
    {
        double sum = 0.0;
        int n1 = v1.size();
        int n2 = v2.size();
        if(n1 == n2)
        {
            for(int i=0;i<n1;i++)
            {
                sum = sum + v1.get(i)*v2.get(i);
            }
        }
        else
        {
            System.out.println("Incomplatible vectors for summation ");
            exit(0);
        }
        return sum;
    }
    public static ArrayList<Double> sub_vec(ArrayList<Double> v1, ArrayList<Double> v2) // Computes the dot product
    {
        ArrayList<Double> res = new ArrayList<Double>();
        double sum = 0.0;
        int n1 = v1.size();
        int n2 = v2.size();
        if(n1 == n2)
        {
            for(int i=0;i<n1;i++)
            {
                res.add(v1.get(i) - v2.get(i));
            }
        }
        else
        {
            System.out.println("Incomplatible vectors for summation ");
            exit(0);
        }
        return res;
    }
    
    public static ArrayList<ArrayList<Double>> mult_matrix(ArrayList<ArrayList<Double>> m1,ArrayList<ArrayList<Double>> m2)
    {
        ArrayList<ArrayList<Double>> m = new ArrayList<ArrayList<Double>>();
        m2 = transpose(m2);
        int n1 = m1.size();
        int n2 = m2.size();
        int n = n1;
        if(n1 == n2)
        {
            // addition is compatible
            for(int i=0;i<n;i++)
            {
                ArrayList<Double> temp = new ArrayList<Double>();
                for(int j=0;j<n;j++)
                {
                    temp.add(dot(m1.get(i),m2.get(j)));
                }
                m.add(temp);
            }
        }
        
        else{
            System.out.println("Incompatible types for matrix multiplication. Please check them ");
            exit(0);
        }
        
        return m;
    }
    
    public static void print_matrix(ArrayList<ArrayList<Double>> m)
    {
        int n = m.size();
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++)
            {
                System.out.print(m.get(i).get(j) + " ");
            }
            System.out.println();
        }
    }
    
    public static ArrayList<ArrayList<Double>> degree_centrality(ArrayList<ArrayList<Double>> graph, double limit)
    {
        ArrayList<ArrayList<Double>> central_graph = new ArrayList<ArrayList<Double>>();
        int l = graph.size();
        
        for(int i=0;i<l;i++)
        {
            ArrayList<Double> temp = new ArrayList<Double>();
            for(int j=0;j<l;j++)
            {
                if(graph.get(i).get(j)>= limit)
                    temp.add(graph.get(i).get(j));
                else
                    temp.add(0.0);
            }
            central_graph.add(temp);
        }
        
        return central_graph;
    }
    
    public static ArrayList<Double> central_arraylist(ArrayList<ArrayList<Double>> graph)
    {
        ArrayList<Double> result = new ArrayList<Double>();
        int l = graph.size();
        
        for(int i=0;i<l;i++)
        {
            double sum = 0.0;
            for(int j=0;j<l;j++)
            {
                sum = sum + graph.get(i).get(j);
            }
            
            result.add(sum);
        }
        
        return result;
    }
    
    public static ArrayList<Double> vec_mat(ArrayList<ArrayList<Double>> m1, ArrayList<Double> v1)
    {
        int n1 = m1.size();
        int n2 = v1.size();
        ArrayList<Double> result = new ArrayList<Double>();
        if(n1 == n2)
        {
            for(int i=0;i<n1;i++)
            {
                result.add(dot(m1.get(i),v1));
            }
        }
        else
        {
            System.out.println("Incompatible types for vector_matrix multiplication");
        }
        
        return result;
    }
    
    // Calculation of eigen vector of eigen value 1 for a given matrix M 
    // M is assumed to be aperiodic and irreducible matrix
    
    public static double norm(ArrayList<Double> v)
    {
        return Math.sqrt(dot(v,v));
    }
    
    public static double sum_vec(ArrayList<Double> v)
    {
        double sum = 0.0;
        for (Double v1 : v) {
            sum = sum + v1;
        }
        return sum;
    }
    
    public static ArrayList<Double> eigen(ArrayList<ArrayList<Double>> m)
    {
        ArrayList<Double> res = new ArrayList<Double>();
        ArrayList<Double> res_prev = new ArrayList<Double>();
        ArrayList<Double> diff = new ArrayList<Double>();
        int n = m.size();
        int i;
        for(i=0;i<n;i++)
        {
            res.add((double) 1/n);
            res_prev.add((double) 1/n);
        }
        // Initialize the eigen vector to all values to 1/n 
        // From now multiply with the first matrix and make it go to 0 with some buffer
        res = vec_mat(m,res_prev);
        diff = sub_vec(res,res_prev);
        int count = 0;
        
        while(norm(diff) > 0.0000001)
        {
            count++;
            res_prev = res;
            res = vec_mat(m,res_prev);
            diff = sub_vec(res,res_prev);
        }
        
       // System.out.println("No. of iterations: " + count);
       // System.out.println("Result"+res);
       // System.out.println("Previous Result"+res_prev);
       // System.out.println("Difference" + diff);
       // System.out.println("the res should be the answer"+vec_mat(m,res));
        
        return res;
    }
    
    public static ArrayList<ArrayList<Double>> Kernal(ArrayList<ArrayList<Double>> B, double d) // here d is the damnping factor
    {
        ArrayList<ArrayList<Double>> K = new ArrayList<ArrayList<Double>>();
        int n = B.size();
        double val = 0.0;
        for(int i=0;i<n;i++)
        {
            ArrayList<Double> temp = new ArrayList<Double>();
            for(int j=0;j<n;j++)
            {
                 val = d*(1/n) + (1-d)*(B.get(i).get(j));
                 temp.add(val);
            }
            K.add(temp);
        }
        //K = transpose(K);
        return K;
    }
    
    public static ArrayList<Double> normalized_vec(ArrayList<Double> v)
    {
        ArrayList<Double> res = new ArrayList<Double>();
        double max = 0.0;
        for(double v1: v)
        {
            if(v1>max) max = v1;
        }
        int i;
        for(i=0;i<v.size();i++)
        {
            
            res.add(v.get(i)/max);
        }
        return res;
    }
    
    public static ArrayList<ArrayList<Double>> B_mat(ArrayList<ArrayList<Double>> A)
    {
        ArrayList<ArrayList<Double>> B = new ArrayList<ArrayList<Double>>();
        int n = A.size();
        double val = 0.0;
        for(int i=0;i<n;i++)
        {
            ArrayList<Double> temp = new ArrayList<Double>();
            for(int j=0;j<n;j++)
            {
                val = A.get(i).get(j)/sum_vec(A.get(i));
                temp.add(val);
            }
            B.add(temp);
        }
        return transpose(B);
    }
    
    public static int find_max_pos(ArrayList<Double> temp)
    {
        int l = temp.size();
        int max_pos = 0;
        double max = 0.0;
        for(int i=0;i<l;i++)
        {
            if(temp.get(i)>max)
            {
                max = temp.get(i);
                max_pos = i;
            }
        }
        
        return max_pos;
    }
    
    public static ArrayList<Integer> sort_position(ArrayList<Double> temp)
    {
        ArrayList<Integer> result = new ArrayList<Integer>();
        int l = temp.size();
        int max_pos = 0;
        for(int i=0;i<l;i++)
        {
            max_pos = find_max_pos(temp);
            temp.set(max_pos,0.0);
            result.add(max_pos);
        }
        
        return result;
    }
     public static ArrayList<Double> row_sum(ArrayList<ArrayList<Double>> m)
    {
        ArrayList<Double> result = new ArrayList<Double>();
        int n = m.size();
        double sum = 0.0;
        for(int i=0;i<n;i++)
        {
           sum = 0.0;
            for(int j=0;j<n;j++)
            {
                sum = sum + m.get(i).get(j);
            }
            result.add(sum);
        }
        return result;
    }

    public static ArrayList<Double> column_sum(ArrayList<ArrayList<Double>> m)
    {
        ArrayList<Double> result = new ArrayList<Double>();
        int n = m.size();
        double sum = 0.0;
        for(int i=0;i<n;i++)
        {
           sum = 0.0;
            for(int j=0;j<n;j++)
            {
                sum = sum + m.get(j).get(i);
            }
            result.add(sum);
        }
        return result;
    }

    public static String final_summary(String input_file, int summ_length) throws IOException
    {
        String summary = "";
        // calculate_idfs();
        read_to_idf("../vocabulary");
        ArrayList<ArrayList<Double>> gg = create_graph(input_file);
        ArrayList<ArrayList<Double>> K = Kernal(B_mat(gg),0.15);
        ArrayList<Double> eigen_vec = eigen(K);

        ArrayList<ArrayList<Double>> result = degree_centrality(gg, 0.2);
        ArrayList<Double> res = central_arraylist(result);

        ArrayList<Integer> sort_list = sort_position(res);

        int length_summary = summ_length;
        for(int kk=0;kk<length_summary;kk++)
        {
            summary = summary + gmap.get(sort_list.get(kk))+".\n";
        }

        return summary;
    }
    
    public static void main(String[] args) throws IOException 
    {
       System.out.println(final_summary("../Airship",7));
    }

}
